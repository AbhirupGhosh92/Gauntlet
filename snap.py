import subprocess


def main_code():
    print("##### Select Options ######")
    print("\n-->runserver {Start A django dev server}"
          "\n-->killserver {Kill the current instance}"
          "\n-->manage {run manage.py without any parameters}"
          "\n-->migrate {run manage.py migrate}"
          "\n-->exit to exit"
          "\n-->2 Future Scope")
    prompt = input()
    if prompt == "runserver":
        subprocess.call(["server/gauntlet_server/shell_scripts/run_django_server.sh"])
    elif prompt == "killserver":
        subprocess.call(["server/gauntlet_server/shell_scripts/kill_django_server.sh"])
    elif prompt == "manage":
        subprocess.call(["server/gauntlet_server/shell_scripts/django_manage.sh"])
    elif prompt == "migrate":
        subprocess.call(["server/gauntlet_server/shell_scripts/django_migrate.sh"])
    elif prompt == "2":
        print("Option not available yet")
        main_code()
    elif prompt == "exit":
        import sys
        sys.exit("Gauntlet stopped by user")


def auto_setup():
    subprocess.call(["server/gauntlet_server/shell_scripts/create_controller_app.sh"])


if __name__ == "__main__":
    subprocess.call(["server/gauntlet_server/shell_scripts/main_shell_controller.sh"])

