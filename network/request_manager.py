def getRequest(url):
    from requests import get
    return get(url)

def postRequest(url,payload):
    from requests import post
    return post(url, data = payload)