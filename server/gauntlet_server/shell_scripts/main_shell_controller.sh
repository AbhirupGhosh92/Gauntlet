#!/usr/bin/env bash


function run_server() {
    python3 server/gauntlet_server/manage.py runserver 8666
}

function generate_migrations() {
    python3 server/gauntlet_server/manage.py makemigrations main_controller
}

function generate_sql() {
    echo "You must pass the migration version as a parameter to the next call"
    read in_prompt
    python3 server/gauntlet_server/manage.py sqlmigrate main_controller ${in_prompt}
}

function create_tables() {
    python3 server/gauntlet_server/manage.py migrate
}

function create_admin_account() {
    python3 server/gauntlet_server/manage.py createsuperuser
}


function initial_run() {
    generate_migrations
    generate_sql
    create_tables
    echo "Basic setup is complete"
    echo "Next we need an admin account, the following steps will create one for you."
    echo "Once admin is created , You can add data from the console to the database"
    create_admin_account
    run_server

}
echo "#######################################################################"
echo "You have just done the snap please select from the following options :-"

sleep .1
options=("Initialize :- If this is your fist time\n or you have made changes to the models or added apps press (1)"
"Run Server :- To start gauntlet server press (2)"
"Manage :- To manage your Gauntlet instance press (3)"
"Make Migrations :- To generate migrations for changes in DB press (4)"
"Generate SQL Queries :- To generate SQL queries for migrations press (5)"
"Migrate :- To migrate changes to DB press (6)"
"exit:- to exit loop")

select opt in "${options[@]}"
do
    case $REPLY in
        "1")
            echo "you chose choice 1"
            echo "Setting up Gauntlet for you"
            initial_run
            ;;
        "2")
            echo "you chose choice 2"
            echo "Starting server"
            run_server
            ;;
        "3")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "4")
            echo "you chose choice $REPLY which is $opt"
            ;;
         "5")
            echo "you chose choice $REPLY which is $opt"
            ;;
         "6")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "exit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
