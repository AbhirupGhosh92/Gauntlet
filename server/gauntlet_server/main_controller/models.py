from django.db import models


# Create your models here.
class Dataset(models.Model):
    dataset_name = models.CharField(max_length=200,primary_key=True)
    dataset_location = models.CharField(max_length=2000,default="",blank=True)
    dataset_file_type = models.CharField(max_length=20,default="",blank=True)
    dataset_op_type = models.CharField(max_length=100,default="",blank=True)
    data_generator_required = models.BooleanField(default=False)
    dataset_train_set_location = models.CharField(max_length=2000,default="",blank=True)
    dataset_test_set_location = models.CharField(max_length=2000,default="",blank=True)
    dataset_validation_set_location = models.CharField(max_length=2000,default="",blank=True)
