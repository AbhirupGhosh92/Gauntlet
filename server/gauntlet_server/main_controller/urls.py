from django.urls import path

from . import views

urlpatterns = [
    path('', views.hello_request, name='hello'),
    path('getData',views.get_data_list,name = "getData")
]