from ...models import Dataset
from django.forms import model_to_dict


def read_db_contents_as_json():
    temp_list = []
    data_list = Dataset.objects.all()
    for data in data_list:
        temp_list.append(model_to_dict(data))

    return temp_list
