from django.http import HttpResponse, JsonResponse
from django.http import HttpRequest
from django.http import HttpResponseServerError,HttpResponseForbidden,HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
import sys
from .modules.data_extractor import django_db_utils


# Create your views here.

def response_parser(payload, status):
    resp_container = {"status": status, "code": 200}
    response_obj = HttpResponse("{}")
    if status == "OK":
        resp_container["payload"] = payload
        response_obj = JsonResponse(resp_container, safe=False)
    elif status == "server_error":
        resp_container["code"] = 500
        response_obj = HttpResponseServerError(resp_container)
    elif status == "not_allowed":
        resp_container["code"] = 403
        response_obj = HttpResponseNotAllowed(permitted_methods="GET")
    return response_obj



def get_context_generator(request, payload):
    status = "OK"
    temp_list = []
    if request.method == "GET":
        try:
            temp_list = payload
        except:
            status = "server_error"
            temp_list = sys.exc_info()[0]
        finally:
            return response_parser(temp_list, status)
    else:
        status = "not_allowed"
    return response_parser(temp_list, status)

@csrf_exempt
def hello_request(request: HttpRequest):
    return HttpResponse("Service Connected")


@csrf_exempt
def get_data_list(request: HttpRequest):
    return get_context_generator(request, django_db_utils.read_db_contents_as_json())
