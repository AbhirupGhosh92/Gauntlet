# Gauntlet
Machine Learning and Artificial Intelligence is booming in every industry. There are lots of open source libraries available which produce highly accurate results. Mega tech companies keep on contributing to this sector day by day.These open source libraries are often so powerful that many enterprise level applications rely on them. This causes a case for concern.



🕷️🕷️🕷️🕷️🕷️🕷️With great power comes great responsibility.🕷️🕷️🕷️🕷️🕷️🕷️                        
                        
                        
We aim to simplify things. We aim to provide all this power to the common man with minimum or no efforts at all. Thus we will incrementally provide simplified libraries which are at par with the current standards but with minimum code complexity and which require almost no prior knowledge to use and play around with.

Our main focus groups are students who are preparing to set off to the unknown world of AI. We want to give them superpowers that they can play with without the hassle of massive configurations and in depth knowledge of AI algos.

We welcome all on board who would like to contribute to this project and make the jouney towards AI a bit more comfortable.

# Basic Architecture

Gauntlet follows a client server model. The Gauntlet server can serve a web application from the cloud or it can serve a local UI client/ 
